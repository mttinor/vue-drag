import Vue from 'vue'
import App from './App.vue'
import axios from "axios"
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import router from '@/router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// import "@syncfusion/ej2-base/styles/material.css";
// import "@syncfusion/ej2-inputs/styles/material.css";
// import "@syncfusion/ej2-vue-dropdowns/styles/material.css";
import vuetify from './plugins/Vuetify'



import BootstrapVueTreeview from 'bootstrap-vue-treeview'
Vue.use(BootstrapVueTreeview)
Vue.config.productionTip = false
// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(axios)
Vue.prototype.$axios = axios
new Vue({
  router,
  vuetify,
  render: h => h(App),
}).$mount('#app')

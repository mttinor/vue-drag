import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);
export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "index",
      component: resolve=> require(['@/components/index'], resolve)
    },
      {
      path: "/instructions",
      name: "instructions",
      component: resolve=> require(['@/components/instructions'], resolve)
    }
  
  ]
});

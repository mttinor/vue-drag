import Vue from 'vue'
import {
  mount
} from '@vue/test-utils';
import BootstrapVue from 'bootstrap-vue'
import Table from '@/views/base/Table'
import Lang from 'vue-lang'
const locales = {
  "en": require("@/locale/en.json"),
  "fa": require("@/locale/fa.json")
}

Vue.use(Lang, {
  lang: 'fa',
  locales: locales
})
Vue.use(BootstrapVue)

describe('Table.vue', () => {

  // it("Should render dynamicButton correctly", async () => {
  //   const buttons = [{
  //       id: '0',
  //       text: "ویرایش",
  //       iconFont: "fa fa-edit fa-lg text-success",
  //       click: () => true
  //     },
  //     {
  //       id: '1',
  //       text: "ویرایش",
  //       iconFont: "fa fa-edit fa-lg text-success",
  //       click: () => true
  //     }
  //   ];
  //   const wrapper = mount(Table, {
  //     propsData: {
  //       dynamicButtons: buttons
  //     }
  //   })
  //   await wrapper.vm.$nextTick();
  //   console.log(wrapper.findAll('.dynamic-buttons'))
  //   expect(wrapper.findAll('.dynamic-buttons').length).toBe(1)

  // })
  // it('has a name', () => {
  //   expect(Table.name).toMatch('c-table')
  // })
  // it('has a created hook', () => {
  //   expect(typeof Table.data).toMatch('function')
  // })
  // it('sets the correct default data', () => {
  //   expect(typeof Table.data).toMatch('function')
  //   const defaultData = Table.data()
  //   expect(defaultData.currentPage).toBe(1)
  // })
  // it('is Vue instance', () => {
  //   const wrapper = mount(Table)
  //   expect(wrapper.isVueInstance()).toBe(true)
  // })
  // it('is Table', () => {
  //   const wrapper = mount(Table)
  //   expect(wrapper.is(Table)).toBe(true)
  // })
  // expect(wrapper.element.value).to.equal('Save draft')


  // it('check foreach btn', () => {


  // const h1 = wrapper.find('h1.mehdi')
  // expect(h1.text()).toBe('mehdi')
  // const firstSwatch = wrapper.findAll('.dynamicButtonGenerated')
  // expect(firstSwatch.classes()).toContain('active')
  // const todosList = wrapper.findAll('div.dynamicButtonGenerated')
  // const div = wrapper.find('#btn-main')
  // console.log(div)
  //  console.log(wrapper.findAll('dynamicButtonGenerated').length)
  // expect(div.length).toBe(1)
  // wrapper.findAll('button.dynamicButtonGenerated').length.toBe(1)
  //  const div = wrapper.find('div')
  //  wrapper.hasClass('dynamicButtonGenerated')
  //  expect(div.isVisible()).toBe(true)
  //  expect(wrapper.findAll(div).length).toBe(1)

  // expect(div.is('div.dynamicButtonGenerated')).toBe(true)
  // expect(wrapper.classes()).toContain('dynamicButtonGenerated')
  // console.log(wrapper.vm)
  // expect(wrapper.classes()).toContain('dynamicButtonGenerated')
  // expect(div.is('div.dynamicButtonGenerated')).toBe(true)
  // expect(wrapper.classes('dynamicButtonGenerated')).toBe(true)
  // expect(wrapper.props('buttons')).toBe('buttons')



  // })

  // test('renders correctly', () => {
  //   const wrapper = shallowMount(Table)
  //   expect(wrapper.element).toMatchSnapshot()
  // })
})

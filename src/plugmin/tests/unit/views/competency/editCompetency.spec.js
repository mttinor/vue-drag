import {
  mount,
  shallowMount,
  createLocalVue
} from '@vue/test-utils';
import Vue from 'vue'

import BootstrapVue from 'bootstrap-vue'
import Vuelidate from 'vuelidate'
import Lang from 'vue-lang'
import selectFind from "@/views/base/selectFind.vue"
import selectAsync from "@/views/base/selectAsync.vue"
import selectAsyncMultple from "@/views/base/selectAsyncMultiple.vue"
import selectFindMultiple from "@/views/base/selectFindMultiple.vue"
import requiredComponent from "@/views/base/required.vue"
import serviceContainer from "@/services"
const locales = {
  "en": require("@/locale/en.json"),
  "fa": require("@/locale/fa.json")
}
Vue.use(Lang, {
  lang: 'fa',
  locales: locales
})
Vue.use(Vuelidate)
Vue.use(BootstrapVue)

import httpService from '@/plugins/httpService'
import appConfig from '@/plugins/appConfig'
import notification from '@/plugins/notification'
Vue.use(httpService)
Vue.use(appConfig)
Vue.use(notification)
Vue.component('selectFind', selectFind)
Vue.component('selectAsync', selectAsync)
Vue.component('selectAsyncMultple', selectAsyncMultple)
Vue.component('selectFindMultiple', selectFindMultiple)
Vue.component('requiredComponent', requiredComponent)
// import Notify from "@/shared/toast";
import flushPromises from 'flush-promises'


jest.mock('vue-router');
import editCompetency from '@/views/competency/editCompetency'
jest.mock("@/services")

const $router = {
  go: jest.fn(),
  push: jest.fn()
}

const $route = {
  path: '/competency/edit/:id',
  params: {
    id: 1
  }
}

describe('editCompetency.vue', async () => {
  it("Should render data correctly", async () => {
    const wrapper = shallowMount(editCompetency, {
      provide: serviceContainer,
      mocks: {
        $route
      }
    });

    let name = 'شایستگی تست';
    wrapper.setData({
      name: name,
    })

    expect(wrapper.find('[data-testid="name"]').attributes().value).toEqual(name);
  })
  it("should render description correctly", async () => {
    const wrapper = shallowMount(editCompetency, {
      provide: serviceContainer,
      mocks: {
        $route
      }
    });

    let description = 'برای تست می باشد';
    wrapper.setData({
      description: description,

    });

    expect(wrapper.find('[data-testid="description"]').attributes().value).toEqual(description);
  })





  it("should submit form", async () => {

    const wrapper = mount(editCompetency, {
      provide: serviceContainer,
      mocks: {
        $router,
        $route
      }
    });
    let name = 'شایستگی تست';

    let description = 'برای تست می باشد';


    wrapper.setData({
      name: name,
      description: description,
      category_selected: {
        value: 1
      },
      reference_selected: {
        value: 1
      },
      showPermissionButton: true

    });
    // await wrapper.vm.$nextTick();
    wrapper.find('[data-testid="submit-button"]').trigger('submit');
    await flushPromises();
    console.log(wrapper.emitted())
    expect(wrapper.emitted('competency-edited')).toBeTruthy()
    // expect(createCompetency).toHaveBeenCalledTimes(1);

  })


  it("should render user description correctly", async () => {
    const wrapper = shallowMount(editCompetency, {
      provide: serviceContainer,
      mocks: {
        $router,
        $route
      }
    });

    let description = wrapper.find('[data-testid="description"]');
    description.setValue('برای تست می باشد')
    let name = wrapper.find('[data-testid="name"]')
    name.setValue('شایستگی تست')






    const category = wrapper.find('[data-testid="category_selected"]').element
    category.value = 1
    category.dispatchEvent(new Event('change'))


    const reference = wrapper.find('[data-testid="reference_selected"]').element
    reference.value = 1
    reference.dispatchEvent(new Event('change'))

    // wrapper.findAll('option').at(1).trigger('change')
    // wrapper.findAll('option').at(1).trigger('change')
    await wrapper.vm.$nextTick();

    wrapper.setData({
      showPermissionButton: true
    })
    wrapper.find('[data-testid="submit-button"]').trigger('submit');
    await flushPromises();
    console.log(wrapper.emitted())
    expect(wrapper.emitted('competency-edited')).toBeTruthy()
  })
})

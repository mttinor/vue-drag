import {
  mount,
  shallowMount,
  createLocalVue
} from '@vue/test-utils';
import Vue from 'vue'

import BootstrapVue from 'bootstrap-vue'
import Vuelidate from 'vuelidate'
import Lang from 'vue-lang'
import selectFind from "@/views/base/selectFind.vue"
import selectAsync from "@/views/base/selectAsync.vue"
import selectAsyncMultple from "@/views/base/selectAsyncMultiple.vue"
import selectFindMultiple from "@/views/base/selectFindMultiple.vue"
import requiredComponent from "@/views/base/required.vue"
import serviceContainer from "@/services"
const locales = {
  "en": require("@/locale/en.json"),
  "fa": require("@/locale/fa.json")
}
Vue.use(Lang, {
  lang: 'fa',
  locales: locales
})
Vue.use(Vuelidate)
Vue.use(BootstrapVue)

import httpService from '@/plugins/httpService'
import appConfig from '@/plugins/appConfig'
import notification from '@/plugins/notification'
Vue.use(httpService)
Vue.use(appConfig)
Vue.use(notification)
Vue.component('selectFind', selectFind)
Vue.component('selectAsync', selectAsync)
Vue.component('selectAsyncMultple', selectAsyncMultple)
Vue.component('selectFindMultiple', selectFindMultiple)
Vue.component('requiredComponent', requiredComponent)
// import Notify from "@/shared/toast";
import flushPromises from 'flush-promises'

const $router = {
  go: jest.fn(),
  push: jest.fn()
}
jest.mock('vue-router');
import addCompetency from '@/views/competency/addCompetency'
jest.mock("@/services")

// jest.mock("@/shared/toast")
// jest.mock("@/services/competency", () => ({
//   createCompetency: () => Promise.resolve({
//     success: true
//   }),
//   getCategory: () => Promise.resolve({
//     success: true
//   }),
//   getReference: () => Promise.resolve({
//     success: true
//   }),
// }))

describe('addCompetency.vue', async () => {

  it("Should render data correctly", async () => {
    const wrapper = shallowMount(addCompetency, {
      provide: serviceContainer
    })

    let name = 'شایستگی تست';

    wrapper.setData({
      name: name,
    })

    expect(wrapper.find('[data-testid="name"]').attributes().value).toEqual(name);
  })
  it("should render description correctly", async () => {
    const wrapper = shallowMount(addCompetency, {
      provide: serviceContainer
    })

    let description = 'برای تست می باشد';
    wrapper.setData({
      description: description,

    })
    await wrapper.vm.$nextTick();
    expect(wrapper.find('[data-testid="description"]').attributes().value).toEqual(description);
  })

  it("should submit form", async () => {

    const wrapper = mount(addCompetency, {
      provide: serviceContainer,
      mocks: {
        $router
      }
    })
    let name = 'شایستگی تست';

    let description = 'برای تست می باشد';
    wrapper.setData({
      name: name,
      description: description,
      category_id: {
        value: 1
      },
      reference_id: {
        value: 1
      }
    })
    await wrapper.vm.$nextTick();
    wrapper.find('[data-testid="submit-button"]').trigger('submit');
    await flushPromises();
    expect(wrapper.emitted('competency-added')).toBeTruthy()
    // expect(createCompetency).toHaveBeenCalledTimes(1);

  })
})

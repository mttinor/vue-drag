# rasa-dashboard

## rasa dashboard with vue js

### Run Steps
- change npm config
    - run `npm config edit` and this variable:
        `chromedriver_cdnurl=https://download.jamko.ir/chromedriver/`
    - `npm i`
    - `npm run serve`

### Deploy
- local: `npm run serve`
- dev: `npm run dev`
- main: `npm run build`

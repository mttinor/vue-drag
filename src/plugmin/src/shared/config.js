export default {

  // base_url: "http://localhost:8080",
  // base_url: "http://ac.dev.rasa-hr.com",
  // base_url: "https://ac.rasa-hr.com",
  //برای مسیر عکس های استاتیک
  base_url: process.env.VUE_APP_BASE_URL,

  // api_url: "http://api.rasa-hr.local/api/v1/",
  // api_url: "http://api.dev.rasa-hr.com/api/v1/",
  // api_url: "https://api.rasa-hr.com/api/v1/",
  api_url: process.env.VUE_APP_API_URL,


  // server_url:"http://api.rasa-hr.local/",
  // server_url:"http://api.dev.rasa-hr.com/",
  // server_url:"http://api.dev.rasa-hr.com/",
  //برای مسیر عکس های سرور
  server_url: process.env.VUE_APP_SERVER_URL,
  client: {
    grant_type: "password",
    client_id: "1",
    // client_secret: "psgF2OEuKZpIVuJMSLMthrG2d8RSPdcK8K0KiWEB",
    client_secret: "GumYWwaRs7C8qpL8jJV08uFFaCq4UrGshUKPZLBG",
    scope: "*"
  },

  resources: {
    auth: 'auth',
    two_step: 'two_step',
    getMenu: 'menu',
    login: 'login',
    person: 'person',
    company: 'company',
    person_company: 'company/{0}/person',
    person_attachment: 'person/{0}/attachment',

    company_person: 'person/{0}/company',
    person_address: 'person/{0}/address',
    person_contact: 'person/{0}/contact',
    company_address: 'company/{0}/address',
    company_contact: 'company/{0}/contact',
    competency: 'competency',
    competency_exercise: 'competency/{0}/exercise',
    competency_indicator: 'competency/{0}/indicator',
    indicator: 'indicator',
    exercise: 'exercise',
    province: "province",
    city: "city",
    exercise_algorithm: "exercise/{0}/algorithm",
    exercise_indicator:"exercise/{0}/indicator",
    exercise_algorithm_detail: "exercise/{0}/algorithm/{1}/detail",
    algorithm: "algorithm",
    algorithm_detail: "detail",
    template: "template",
    assessment_plan: "assessment_plan",
    assessment_plan_data: "assessment_plan/{0}/data",
    assessment_plan_pseudo_data: "assessment_plan/{0}/pseudo_data",
    enum: "enum",
    enum_grouped: "enum/grouped",
    center_execute: "center_execute",
    day_center_room: "room",
    user: "user",
    user_info: "user_info",
    user_role: "user/{}/role",
    role: "role",
    role_permissions: "role/{role_id}/permissions",
    role_duplicate: "role/{role_id}/duplicate",
    role_users: "role/{role_id}/users",
    role_tree: "role_tree",
    upload_folder: "upload-file",


    template_duplicate: "template/{0}/duplicate",
    template_competency: 'template/{0}/competency',
    template_competency_indicator: 'template/{0}/competency/{competency_id}/indicator',
    template_competency_indicator_exercise: 'template/{0}/competency/{1}/indicator/{2}/exercise',
    template_indicator: 'template/{0}/indicator',
    template_indicator_exercise: 'template/{0}/indicator/{indicator_id}/exercise/{exercise_id}',
    template_exercise: 'template/{0}/exercise/{1}',

    center_execute_person: 'center_execute/{0}/person',
    center_execute_executive: 'center_execute/{0}/executive',
    center_execute_room: 'center_execute/{0}/room',
    center_execute_case: 'center_execute/{0}/case',
    center_execute_case_competency: 'center_execute/{0}/case/{1}/competency',
    center_execute_case_evidence: 'center_execute/{0}/case/{1}/tcie/{2}/evidence',
    center_execute_case_evidences: 'center_execute/{0}/case/{1}/evidence',
    center_execute_case_evidences_by_competency: 'center_execute/{0}/case/{1}/competency/{2}/evidence',
    evidence: 'evidence',

    washup: 'center_execute/{0}/washup',
    washup_details: 'center_execute/{center_execute_id}/washup/candidate/{center_execute_candidate_id}',
    washup_competency_details: 'center_execute/{center_execute_id}/washup/candidate/{center_execute_candidate_id}/competency/{competency_id}',
    filterData: "filter/{key_filter_name}"
  },
  metaData: {
    gender: "enum/gender",
    marriage: "enum/marriage",
    competency_category: "enum/competency_category",
    competency_reference: "enum/competency_reference",
    exercise_category: "enum/exercise_category",
    template_category: "enum/template_category",
    contact_kind: "enum/contact_kind",
    person_address_kind: "enum/person_address_kind",
    company_address_kind: "enum/company_address_kind",
    enum_grouped: "enum/grouped",
    center_execute_role: "enum/center_execute_role",

    

    error_code: "enum/error_code",
    permission_group: "permission_group",
    sample_profile_pic: "no-person.jpg",
    center_execute_status: {
      in_progress: 3,
    },
    enum: 'enum',
    captcha: 'captcha/api/math',

    entity_enum_builder: "enum/{entity}_{enum}",
  },
  form: {
    labelCol: 12,
    textareaRows: 4,
  },
  login: {
    verification_code_timeout: 180, // حداکثر زمان لازم برای ارسال کد اعتبارسنجی
    two_step_verification_timeout: 180, // حداکثر زمان لازم برای ارسال کد تایید دو مرحله ای
    backgournd_image_count: 2 // تعداد عکس های بک گراند صفحه ورود
  },
  role_guard_name:'api'
};

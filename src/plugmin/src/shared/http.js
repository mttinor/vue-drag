// shaghouzi
/*

بر مبنای httpcode پیاده سازی شود
تصمیم گرفته شود به نوتیفیکیشن وصل شود یا نه

*/
import axios from 'axios';
axios.defaults.headers.common['Accept'] = 'application/json'
axios.defaults.headers.common['Content-Type'] = 'application/json'

import Notification from './toast';
import store from './../store/index';
import router from "../router"
// import locale from "../locale/fa.json"

import lang from '../locale/fa.json'
// const lang= JSON.parse(locale);
// 


// check how axios status when get post put call


// axios.interceptors.response.use((response) => {
//   return response;
// }, error => {
//   if (error.response.status === 404) {
//       console.log('unauthorized, logging out ...');
//       auth.logout();
//       router.replace('/auth/login');
//   }
//   return Promise.reject(error.response);
// });

class httpService {


  static fillHeaders(headers = {}) {
    let token = this.getToken();
    if (headers) {
      Object.keys(headers).map(header => {
        token.headers[header] = headers[header];
      });
    }
    console.log(token);
    return token;
  }

  static getToken() {
    if (!localStorage.getItem('accessToken'))
      this.logout();
    let config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
      }
    }
    return config
  }

  static get(url, params, headers = {}, token = true) {
    // alert('url==='+url);
    if (token) {
      headers = this.fillHeaders(headers);
      if (!headers)
        this.logout();
    }
    if (!params) {
      headers.params = params
    }
    return this.response(axios.get(url, headers));
  }

  static post(url, data, headers = {}, token = true, config = {}) {
    if (token) {
      headers = this.fillHeaders(headers);
      if (!headers)
        return false;
    }
    return this.response(axios.post(url, data, {
      ...headers,
      ...config
    }));
  }

  static put(url, data, headers = {}, token = true) {
    if (token) {
      headers = this.fillHeaders(headers);
      if (!headers)
        return false;
    }
    return this.response(axios.put(url, data, headers));
  }

  static patch(url, data, headers = {}, token = true) {
    if (token) {
      headers = this.fillHeaders(headers);
      if (!headers)
        return false;
    }
    return this.response(axios.patch(url, data, headers));
  }

  static delete(url, data, headers = {}, token = true) {
    if (token) {
      headers = this.fillHeaders(headers);
      if (!headers)
        return false;
    }
    console.log(headers);
    return this.response(axios.delete(url, headers));
  }

  static response(res) {
    return res.then(response => {
      // console.log(response);
      return response;
    }).catch(error => {
      if (error.response.status === 401) {
        this.logout();
      } else if (error.response.status === 403) {
        this.forbidden()
      }
      throw error;
    });

  }
  static forbidden() {
    Notification.error(lang.error_forbidden);
    router.push({
      name: "Dashboard"
    })
  }
  static logout() {
    // console.log("log out")
    // store.commit("logout")
    store.dispatch('logout')
  }
}

export default httpService;

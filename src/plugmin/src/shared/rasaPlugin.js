import Vue from 'vue'
export default {
  install() {
    Vue.helpers = helpers
    Vue.prototype.$helpers = helpers
  }
}

import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/index'
// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer');

// Views
const HomePage = () => import('@/views/HomePage');

// Views - Auth
const Login = () => import('@/views/auth/Login');
const Register = () => import('@/views/auth/Register');
// const search = () => import('@/views/search');
const searchCompetency = () => import('@/views/competency/searchCompetency');
const searchIndicator = () => import('@/views/indicator/searchIndicator');
const searchExercise = () => import('@/views/exercise/searchExercise');
const exerciseDesignPage = () => import('@/views/exercise/exercise_design/exerciseDesignPage');
const searchAlgorithm = () => import('@/views/exercise/algorithm/searchAlgorithm');
// const searchAlgorithmDetail = () => import('@/views/exercise/algorithm/detail/searchAlgorithmDetail');
const searchPerson = () => import('@/views/person/searchPerson');
const searchCompany = () => import('@/views/company/searchCompany');
const searchUser = () => import('@/views/user/searchUser');
const searchTemplate = () => import('@/views/template/searchTemplate');
const searchAssessment_plan = () => import('@/views/assessment_plan/searchAssessment_plan');
const SearchCenter_execute = () => import('@/views/center_execute/searchCenter_execute');
const SearchEnum = () => import('@/views/enum/searchEnum');
const SearchRole = () => import('@/views/role/searchRole');
const SearchDayCenterRoom = () => import('@/views/day_center_room/searchDayCenterRoom');
const SearchCenterExecuteCase = () => import('@/views/center_execute_case/searchCenterExecuteCase');
const SearchCenterExecuteCaseCompetency = () => import('@/views/center_execute_case_competency/searchCenterExecuteCaseCompetency');
const SearchWashUp = () => import('@/views/wash_up/searchWashUp');

const AddCompany = () => import('@/views/company/addCompany');
const AddPerson = () => import('@/views/person/addPersonNew');
const AddCompetency = () => import('@/views/competency/addCompetency');
const AddIndicator = () => import('@/views/indicator/addIndicator');
const AddExercise = () => import('@/views/exercise/addExercise');
const AddAlgorithm = () => import('@/views/exercise/algorithm/addAlgorithm');
// const AddAlgorithmDetail = () => import('@/views/exercise/algorithm/detail/addAlgorithmDetail');
const AddTemplate = () => import('@/views/template/addTemplate');
const AddAssessment_plan = () => import('@/views/assessment_plan/addAssessment_plan');
const AddEnum = () => import('@/views/enum/addEnum');
const AddCenter_execute = () => import('@/views/center_execute/addCenter_execute');
const AddUser = () => import('@/views/user/addUser');
const AddRole = () => import('@/views/role/addRole');
const AssignRolePermissions = () => import('@/views/role/assignRolePermissions');
const AddDayCenterRoom = () => import('@/views/day_center_room/addDayCenterRoom');

const editCompetency = () => import('@/views/competency/editCompetency');
const editExercise = () => import('@/views/exercise/editExercise');
const editAlgorithm = () => import('@/views/exercise/algorithm/editAlgorithm');
// const editAlgorithmDetail = () => import('@/views/exercise/algorithm/detail/editAlgorithmDetail');
const EditIndicator = () => import('@/views/indicator/editIndicator');
const EditEnum = () => import('@/views/enum/editEnum');
const editPerson = () => import('@/views/person/editPerson');
const editUser = () => import('@/views/user/editUser');
const changePassword = () => import('@/views/user/changePass');
const editCompany = () => import('@/views/company/editCompany');
const editTemplate = () => import('@/views/template/editTemplate');
const EditAssessment_plan = () => import('@/views/assessment_plan/editAssessment_plan');
const EditCenter_execute = () => import('@/views/center_execute/editCenter_execute');
const EditRole = () => import('@/views/role/editRole');
const EditDayCenterRoom = () => import('@/views/day_center_room/editDayCenterRoom');
const EditCenterExecuteCaseCompetency = () => import('@/views/center_execute_case_competency/editCenterExecuteCaseCompetency');
const EditWashUp = () => import('@/views/wash_up/editWashUp');
const select = () => import('@/views/base/selectSearch');
const assessmentPlanPseudoData = () => import('@/views/assessment_plan_data/assessmentPlanPseudoData');
const routerParrent = () => import('@/views/base/routerParrent');
const schedulerPage = () => import('@/views/scheduler/schedulerPage');
const treeView = () => import('@/views/base/treeView');
const roleTree = () => import('@/views/role/roleTree');


Vue.use(Router);

function configRoutes() {
  return [{
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      meta: {
        label: "خانه"
      },
      component: DefaultContainer,
      children: [{
          path: 'dashboard',
          name: 'Dashboard',
          label: "داشبورد",
          component: HomePage,
          meta: {
            label: "داشبورد"
          }
        },
        {
          path: 'company',
          name: 'companyHome',
          meta: {
            label: "شرکت"
          },
          component: routerParrent,
          redirect: '/company/search',
          children: [{
              path: 'add',
              name: 'addCompany',
              component: AddCompany,
              meta: {
                label: "ثبت شرکت"
              },
            },
            {
              path: 'edit/:id',
              name: 'editCompany',
              component: editCompany,
              meta: {
                label: "ویرایش شرکت"
              },
            },
            {
              path: 'search',
              name: 'companySearch',
              component: searchCompany,
              meta: {
                label: "جستجوی شرکت"
              },
            },
          ]
        },
        {
          path: 'exercise',
          name: 'exercise',
          redirect: '/exercise/search',
          component: routerParrent,
          meta: {
            label: "تمرین"
          },
          children: [{
              path: 'add',
              name: 'addExercise',
              component: AddExercise,
              meta: {
                label: "ایجاد تمرین"
              },
            }, {
              path: 'search',
              name: 'exerciseSearch',
              component: searchExercise,
              meta: {
                label: "جستجوی تمرین"
              },
            }, {
              path: 'edit/:id',
              name: 'editExercise',
              component: editExercise,
              meta: {
                label: "ویرایش تمرین"
              },

            },
            {
              path: 'exerciseDesign/:id',
              name: 'exerciseDesign',
              component: exerciseDesignPage,
              meta: {
                label: "طراحی تمرین"
              }

            },
            {
              path: ':id/algorithm',
              name: 'algorithm',
              redirect: '/exercise/:id/algorithm/search',
              component: routerParrent,
              meta: {
                label: "الگوریتم تمرین"
              },
              children: [{
                path: 'search',
                name: 'searchAlgorithm',
                component: searchAlgorithm,
                meta: {
                  label: "جستجوی الگوریتم تمرین"
                },
              }, {

                path: 'add',
                name: 'AddAlgorithm',
                component: AddAlgorithm,
                meta: {
                  label: "الگوریتم تمرین"
                },
              }, {
                path: 'edit/:algorithm_id',
                name: 'editAlgorithm',
                component: editAlgorithm,
                meta: {
                  label: "ویرایش الگوریتم تمرین"
                },
              }, ]
            }

          ]
        },
        {
          path: 'assessment_plan',
          name: 'assessment_plan',
          redirect: '/assessment_plan/search',
          component: routerParrent,
          meta: {
            label: "برنامه ارزیابی"
          },
          children: [{
            path: 'search',
            name: 'searchAssessment_plan',
            component: searchAssessment_plan,
            meta: {
              label: "جستجوی برنامه ارزیابی"
            },
          }, {
            path: 'add',
            name: 'AddAssessment_plan',
            component: AddAssessment_plan,
            meta: {
              label: "ایجاد برنامه ارزیابی"
            },
          }, {
            path: 'edit/:id',
            name: 'EditAssessment_plan',
            component: EditAssessment_plan,
            meta: {
              label: "ویرایش برنامه ارزیابی"
            },
          }, {
            path: 'pseudo_data/:id',
            name: 'assessment_plan_pseudo_data',
            component: assessmentPlanPseudoData,
            meta: {
              label: "داده های برنامه ارزیابی"
            },
          }, ]

        },

        {
          path: 'center_execute',
          name: 'centerExecute',
          redirect: '/center_execute/search',
          component: routerParrent,
          meta: {
            label: "کانون"
          },
          children: [{
              path: 'search',
              name: 'SearchCenter_execute',
              component: SearchCenter_execute,
              meta: {
                label: "جستجوی کانون"
              },
            },
            {
              path: 'add',
              name: 'AddCenter_execute',
              component: AddCenter_execute,
              meta: {
                label: "ثبت کانون"
              },
            },
            {
              path: 'edit/:id',
              name: 'EditCenter_execute',
              component: EditCenter_execute,
              meta: {
                label: "ویرایش کانون"
              },
            },
            {
              path: ':center_execute_id/case',
              name: 'centerExecuteCase',
              redirect: ':center_execute_id/case/search',
              component: routerParrent,
              meta: {
                label: "پرونده های کانون"
              },
              children: [{
                  path: 'search',
                  name: 'searchCenterExecuteCase',
                  component: SearchCenterExecuteCase,
                  meta: {
                    label: "جستجوی پرونده ها"
                  },
                },
                {
                  path: ':case_id/competency',
                  name: 'searchCenterExecuteCaseCompetency',
                  component: routerParrent,
                  redirect: ':case_id/competency/search',
                  meta: {
                    label: "شایستگی ها"
                  },
                  children: [{
                      path: 'search',
                      name: 'searchCenterExecuteCaseCompetency',
                      component: SearchCenterExecuteCaseCompetency,
                      meta: {
                        label: "جستجوی شایستگی ها"
                      },
                    },
                    {
                      path: ':competency_id/score',
                      name: 'editCenterExecuteCaseCompetency',
                      component: EditCenterExecuteCaseCompetency,
                      meta: {
                        label: "امتیاز دهی شایستگی"
                      },
                    },
                  ]
                },

              ]
            },
            {
              path: ':center_execute_id/washup',
              redirect: ':center_execute_id/washup/search',
              name: 'washUp',
              component: routerParrent,
              meta: {
                label: "واش‌آپ"
              },
              children: [{
                path: 'search',
                name: 'searchWashUp',
                component: SearchWashUp,
                meta: {
                  label: "جستجوی واش‌آپ"
                },
              }, {
                path: ':center_execute_candidate_id/edit',
                name: 'editWashUp',
                component: EditWashUp,
                meta: {
                  label: "ویرایش واش‌آپ"
                },
              }, ]
            },

          ]
        },

        {
          path: 'users',
          name: 'users',
          component: routerParrent,
          redirect: '/users/search',
          meta: {
            label: "کاربر"
          },
          children: [{
            path: 'search',
            name: 'searchUser',
            component: searchUser,
            meta: {
              label: "جستجوی کاربر"
            },
          }, {
            path: 'add',
            name: 'AddUser',
            component: AddUser,
            meta: {
              label: "درج کاربر"
            },
          }, {
            path: 'edit/:id',
            name: 'editUser',
            component: editUser,
            meta: {
              label: "ویرایش کاربر"
            },
          }, {
            path: 'change_pass/:id',
            name: 'changePassword',
            component: changePassword,
            meta: {
              label: "تغییر گذرواژه"
            },
          }, ]
        },
        {
          path: 'competency',
          redirect: '/competency/search',
          name: 'competency',
          component: routerParrent,
          meta: {
            label: "شایستگی"
          },
          children: [{
            path: 'search',
            name: 'searchCompetency',
            component: searchCompetency,
            meta: {
              label: "جستجوی شایستگی"
            },
          }, {
            path: 'add',
            name: 'addCompetency',
            component: AddCompetency,
            meta: {
              label: "ایجاد شایستگی"
            },
          }, {
            path: 'edit/:id',
            name: 'editCompetency',
            component: editCompetency,
            meta: {
              label: "ویرایش شایستگی"
            },
          }, ]
        },

        {
          path: 'person',
          name: 'person',
          component: routerParrent,
          redirect: '/person/search',
          meta: {
            label: "شخص"
          },
          children: [{
            path: 'search',
            name: 'searchPerson',
            component: searchPerson,
            meta: {
              label: "جستجوی شخص"
            },
          }, {
            path: 'add',
            name: 'addPerson',
            component: AddPerson,
            meta: {
              label: "ثبت شخص"
            },
          }, {
            path: 'edit/:id',
            name: 'editPerson',
            component: editPerson,
            meta: {
              label: "ویرایش شخص"
            },
          }, ]
        },

        {
          path: 'indicator',
          name: 'indicator',
          redirect: '/indicator/search',
          component: routerParrent,
          meta: {
            label: "نشانگر"
          },
          children: [{
            path: 'add',
            name: 'addIndicator',
            component: AddIndicator,
            meta: {
              label: "درج نشانگر"
            },
          }, {
            path: 'edit/:id',
            name: 'EditIndicator',
            component: EditIndicator,
            meta: {
              label: "ویرایش نشانگر"
            },
          }, {
            path: 'search',
            name: 'searchIndicator',
            component: searchIndicator,
            meta: {
              label: "جستجوی نشانگر"
            },
          }, ]
        },

        {
          path: 'template',
          name: 'template',
          redirect: '/template/search',
          component: routerParrent,
          meta: {
            label: "قالب"
          },
          children: [{
            path: 'search',
            name: 'searchTemplate',
            component: searchTemplate,
            meta: {
              label: "جستجوی قالب"
            },
          }, {
            path: 'add',
            name: 'AddTemplate',
            component: AddTemplate,
            meta: {
              label: "درج قالب"
            },
          }, {
            path: 'edit/:id',
            name: 'editTemplate',
            component: editTemplate,
            meta: {
              label: "ویرایش قالب"
            },
          }, ]
        },
        {
          path: 'enumeration',
          name: 'enumeration',
          redirect: '/enumeration/search',
          component: routerParrent,
          meta: {
            label: "مقادیر پایه"
          },
          children: [{
            path: 'search',
            name: 'searchEnum',
            component: SearchEnum,
            meta: {
              label: "جستجوی مقادیر پایه"
            },
          }, {
            path: 'add',
            name: 'addEnum',
            component: AddEnum,
            meta: {
              label: "درج مقادیر پایه"
            },
          }, {
            path: 'edit/:id',
            name: 'editEnum',
            component: EditEnum,
            meta: {
              label: "ویرایش مقادیر پایه"
            },
          }, ]
        },

        {
          path: 'role',
          name: 'role',
          redirect: '/role/search',
          component: routerParrent,
          meta: {
            label: "نقش"
          },
          children: [{
            path: 'search',
            name: 'searchRole',
            // component: SearchRole,
            component: roleTree,
            meta: {
              label: "جستجوی نقش"
            },
          }, {
            path: 'add',
            name: 'addRole',
            component: AddRole,
            meta: {
              label: "درج نقش"
            },
          }, {
            path: 'edit/:id',
            name: 'editRole',
            component: EditRole,
            meta: {
              label: "ویرایش نقش"
            },
          }, {
            path: ':id/permissions',
            name: 'assignRolePermissions',
            component: AssignRolePermissions,
            meta: {
              label: "دسترسی های نقش"
            },
          }, ]
        },

        {
          path: 'day_center_room',
          name: 'day_center_room',
          redirect: '/day_center_room/search',
          component: routerParrent,
          meta: {
            label: "اتاق های روز کانون"
          },
          children: [{
              path: 'search',
              name: 'searchDayCenterRoom',
              component: SearchDayCenterRoom,
              meta: {
                label: "جستجوی اتاق های روز کانون"
              },
            },
            {
              path: 'add',
              name: 'addDayCenterRoom',
              component: AddDayCenterRoom,
              meta: {
                label: "درج اتاق های روز کانون"
              },
            },
            {
              path: 'edit/:id',
              name: 'editDayCenterRoom',
              component: EditDayCenterRoom,
              meta: {
                label: "ویرایش اتاق های روز کانون"
              },
            },

          ]
        },
        {
          path: 'scheduler',
          name: 'scheduler',
          redirect: '/scheduler/schedulerPage',
          component: routerParrent,
          meta: {
            label: "جدول برنامه ریزی"
          },
          children: [{
              path: 'schedulerPage',
              name: 'schedulerPage',
              component: schedulerPage,
              meta: {
                label: "برنامه زمانی"
              },
            },
            // {
            //   path: 'add',
            //   name: 'addDayCenterRoom',
            //   component: AddDayCenterRoom,
            //   meta: {
            //     label: "درج اتاق های روز کانون"
            //   },
            // },
            // {
            //   path: 'edit/:id',
            //   name: 'editDayCenterRoom',
            //   component: EditDayCenterRoom,
            //   meta: {
            //     label: "ویرایش اتاق های روز کانون"
            //   },
            // },
          ]
        }
      ]
    },
    {
      path: '/auth',
      redirect: '/auth/login',
      name: 'Auth',
      component: {
        render(c) {
          return c('router-view')
        }
      },
      children: [{
          path: 'login',
          name: 'Login',
          component: Login
        },
        {
          path: 'register',
          name: 'Register',
          component: Register
        },
        {
          path: 'forget-password',
          name: 'forget-password',
          component: Register
        }
      ]
    }
  ];
}

const route = new Router({
  // mode: 'hash', // https://router.vuejs.org/api/#mode
  mode: 'history',
  base: process.env.BASE_URL,
  linkActiveClass: 'open active',
  scrollBehavior: () => ({
    y: 0
  }),
  routes: configRoutes()
});
route.beforeEach((to, from, next) => {
  store.dispatch('fetchAccessToken');
  if (to.fullPath !== '/auth/login') {
    if (!store.state.accessToken) {
      next('/auth/login');
    }
  }
  if (to.fullPath === '/auth/login') {
    if (store.state.accessToken) {
      next('/');
    }
  }
  next();
});
export default route;

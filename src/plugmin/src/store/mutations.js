export default {
  loginStart: state => state.loggingIn = true,
  loginStop: (state, errorMessage) => {
    state.loggingIn = false;
    state.loginError = errorMessage;
  },
  updateAccessToken: (state, accessToken) => {
    state.accessToken = accessToken;
  },
  logout: (state) => {
    state.accessToken = null;
  },
  updateSearchConfig(state, config) {
    let [name] = Object.keys(config);
    state.searchConfig[name] = config[name]
  },
  updateUserInfo(state, userInfo) {
    state.userInfo = userInfo;
  }
}

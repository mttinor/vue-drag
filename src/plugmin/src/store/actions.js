import httpService from './../shared/http';
import router from './../router/index';
import config from './../shared/config'

export default {
  doLogin({
    commit
  }, loginData) {
    console.log("mode", loginData['mode']);
    commit('loginStart');

    let payload = {
      ...loginData,
      ...config.client
    }
    console.log(loginData)
    let url = '';
    if (loginData['mode'] == 'login') {
      url = config.resources.auth
    } else if (loginData['mode'] == 'two_step') {
      url = config.resources.two_step
    }
    return httpService.post(config.api_url + url, payload, false)
      .then((response) => {
        if ('access_token' in response.data.data) {
          localStorage.setItem('accessToken', response.data.data.access_token);

          commit('loginStop', null)
          commit('updateAccessToken', response.data.data.access_token);
        }
        return response.data;
      })
      .catch(error => {

        commit('loginStop', error.response.data.error)
        commit('updateAccessToken', null);
        throw error;
      });
  },
  fetchAccessToken({
    commit
  }) {
    commit('updateAccessToken', localStorage.getItem('accessToken'));
  },
  logout({
    commit
  }) {
    localStorage.removeItem('accessToken');
    commit('logout');
    router.push('/auth/login');
  },
  changeSearchConfig({
    commit
  }, config) {

    commit('updateSearchConfig', config);
  },
  getUserInfo({
    commit
  }) {
    httpService
      .get(config.api_url + config.resources.user_info)
      .then(response => {
        commit('updateUserInfo', response.data.data)
      })
      .catch(err => {
        if (err.response) {
          console.log(err.response.data.message);
        } else {
          console.log(err.message);
        }
      });
  }
}

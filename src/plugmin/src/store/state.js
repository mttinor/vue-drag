export default {
    loggingIn: false,
    loginError: null,
    accessToken: null,
    config: {},
    searchConfig: {},
    userInfo: {}
  }
import Notify from '../shared/toast'
let notify = {
  install(Vue, options) {
    Vue.prototype.$notify = Notify
  }
}

export default notify;

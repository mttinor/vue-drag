import http from '../shared/http'
let httpPlugin = {
  install(Vue, options) {
    Vue.prototype.$httpService = http
  }
}

export default httpPlugin;

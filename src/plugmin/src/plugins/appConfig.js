import config from '../shared/config'
let appConfig = {
  install(Vue, options) {
    let app = {
        ...config,
        ...options
    }
    Vue.prototype.$config = app
  }
}

export default appConfig;

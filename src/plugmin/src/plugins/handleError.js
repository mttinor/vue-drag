let handleError = {
  install(Vue, options) {
    const handleError = (err) => {
      let errors = [];
      if (
        err.response &&
        "status" in err.response &&
        err.response.status == 400
      ) {
        if (typeof err.response.data.message === "object") {
          err.response.data.message.forEach(element => {
            errors.push(element);
          });
        } else {
          Vue.prototype.$notify.error(err.response.data.message);
        }
      } else {
        Vue.prototype.$notify.error(err.message);
      }
      return errors;
    }
    Vue.prototype.$handleError = handleError
  }
}

export default handleError;

// import CompetencyRepo from './competency.repo';

import {
  BIconAppIndicator
} from "bootstrap-vue"

// // import UserRepository from './UserRepository';

// const repositories = {
//   'competency': CompetencyRepo,
//   // 'users': UserRepository
// }

// export default {
//   get: name => repositories[name]
// };


const RepositoryInterface = {
  list() {
    return Promise.resolve({
      success: true,
      data: {
        data: [{
          value: 1,
          text:'test'
        }, {}]
      }
    })
  },
  create(payload) {
    let data = {
      message: "عملیات با موفقیت انجام شد.",
      state: true
    }
    return Promise.resolve({
      success: true,
      data: data,
      response: {
        data: {
          message: 'error'
        }
      }
    })
  },
  edit(id, payload) {
    let data = {
      message: "عملیات با موفقیت انجام شد.",
      state: true,
    }
    return Promise.resolve({
      success: true,
      data: data,
      response: {
        data
      }
    })
  },
  find(id) {
    return Promise.resolve({
      success: true,
      data: {
        data: {},
        permissions: ['admin']
      }
    })
  },
}

// function bind(repositoryName, Interface) {
function bind() {
  return {
    ...RepositoryInterface
  };
}

export default {
  get competencyRepository() {
    return bind();
  },
  get enumRepository() {
    return bind();
  },
};

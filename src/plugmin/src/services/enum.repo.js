import Vue from 'vue'
import http from '@/shared/http'
import config from '@/shared/config'

const enumRepository = {
  // get() {
  //   return true;
  // },
  find(id) {
    let data = {
      message: "عملیات با موفقیت انجام شد.",
      state: true
    }
    return Promise.resolve({
      success: true,
      data: data,
    })
  },
  edit(id, payload) {
    let data = {
      message: "عملیات با موفقیت انجام شد.",
      state: true
    }
    return Promise.resolve({
      success: true,
      data: data,
    })
  },
  list(key = '', params = {}) {
    console.log(key)
    let uri = config.metaData.enum
    if (key)
      uri += '/' + key
    if (Object.keys(params).length !== 0)
      uri = params;
    console.log(uri)
    return http.get(config.api_url + uri)
  },
}

export default enumRepository;

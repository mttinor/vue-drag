// import CompetencyRepo from './competency.repo';

// // import UserRepository from './UserRepository';

// const repositories = {
//   'competency': CompetencyRepo,
//   // 'users': UserRepository
// }

// export default {
//   get: name => repositories[name]
// };


const RepositoryInterface = {
  find() {},
  list() {},
  create() {},
  edit() {},
  delete() {}
};

// function bind(repositoryName, Interface) {
function bind(repositoryFactory, Interface) {
  return {
    ...Object.keys(Interface).reduce((prev, method) => {
      const resolveableMethod = async (...args) => {
        const repository = await repositoryFactory();
        // const repository = await import(`./repositories/${repositoryName}`);
        return repository.default[method](...args);
      };
      return {
        ...prev,
        [method]: resolveableMethod
      };
    }, {}),
  };
}

export default {
  get competencyRepository() {
    // Delay loading until a method of the repository is called.
    return bind(() => import('./competency.repo'), RepositoryInterface);
  },
  get enumRepository() {
    return bind(() => import('./enum.repo'), RepositoryInterface);
  },
  //   get userRepository() {
  //     // Load the repository immediately when it's injected.
  //     const userRepositoryPromise = import('./user');
  //     return bind(() => userRepositoryPromise, RepositoryInterface);
  //   },
};

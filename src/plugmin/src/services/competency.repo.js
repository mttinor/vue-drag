import Vue from 'vue'
import http from '@/shared/http'
import config from '@/shared/config'

const competency = {
  // get() {
  //   return true;
  // },
  create(payload) {
    return http
      .post(config.api_url + config.resources.competency, payload)
  },
  edit(id, payload) {
    return http
      .put(config.api_url + config.resources.competency + "/" + id, payload)
  },
  find(id) {
    return http
      .get(config.api_url + config.resources.competency + "/" + id)
  },
  getCategory() {
    return http.get(config.api_url + config.metaData.competency_category)
  },
  getReference() {
    return http.get(config.api_url + config.metaData.competency_reference)
  }
}

export default competency;

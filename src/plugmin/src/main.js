// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'core-js/es6/promise'
import 'core-js/es6/string'
import 'core-js/es7/array'
// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import store from './store/index';

// todosdas
// cssVars()
import Vuelidate from 'vuelidate'
import Lang from 'vue-lang'
import selectFind from "./views/base/selectFind.vue"
import selectAsync from "./views/base/selectAsync.vue"
import selectAsyncMultple from "./views/base/selectAsyncMultiple.vue"
import selectFindMultiple from "./views/base/selectFindMultiple.vue"
import requiredComponent from "./views/base/required.vue"
import advanceFilter from "./views/base/advanceFilter"
import CKEditor from '@ckeditor/ckeditor5-vue';
import BootstrapVueTreeview from 'bootstrap-vue-treeview'


import serviceContainer from "@/services"



const locales = {
  "en": require("./locale/en.json"),
  "fa": require("./locale/fa.json")
}
Vue.use(CKEditor);
Vue.use(Lang, {
  lang: 'fa',
  locales: locales
})
Vue.use(Vuelidate)
Vue.use(BootstrapVue)
Vue.use(BootstrapVueTreeview)

import httpService from './plugins/httpService'
import appConfig from './plugins/appConfig'
import notification from './plugins/notification'
import handleError from './plugins/handleError'
Vue.use(httpService)
Vue.use(appConfig)
Vue.use(notification)
Vue.use(handleError)



Vue.component('selectFind', selectFind)
Vue.component('selectAsync', selectAsync)
Vue.component('selectAsyncMultple', selectAsyncMultple)
Vue.component('selectFindMultiple', selectFindMultiple)
Vue.component('requiredComponent', requiredComponent)
Vue.component('advanceFilter', advanceFilter)

console.log(process.env)
new Vue({
  provide: serviceContainer,
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App,
  },
  store
})
